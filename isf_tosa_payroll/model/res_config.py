import time
import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from os.path import join as opj

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging

_logger = logging.getLogger(__name__)
_debug=False

class isf_tosa_payroll_config_settings(osv.osv_memory):
    _name = 'isf.tosa.payroll.config.settings'
    _inherit = 'res.config.settings'
    
    _columns = {
        'advances_journal_id' : fields.many2one('account.journal','Payroll Advances Journal',required=True),
        'advances_account_id' : fields.many2one('account.account','Advance On Payroll Account', required=True),
        'account_expense_gross_salaries_id' : fields.many2one('account.account','Wages & Salaries Expense Account', required=True),
    }
    
    # Set method
	
    def set_advances_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll', 'advances_journal_id',config.advances_journal_id.id)
    
    def set_advances_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll', 'advances_account_id',config.advances_account_id.id)
        
    def set_account_expense_gross_salaries_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll', 'account_expense_gross_salaries_id',config.account_expense_gross_salaries_id.id)
            
    # Get Method
    
    def get_default_advances_journal_id(self, cr, uid, ids, context=None):
        advances_journal_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.tosa.payroll','advances_journal_id')
        return {'advances_journal_id' : advances_journal_id }
        
    def get_default_advances_account_id(self, cr, uid, ids, context=None):
        advances_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.tosa.payroll','advances_account_id')
        return {'advances_account_id' : advances_account_id }
    
    def get_default_account_expense_gross_salaries_id(self, cr, uid, ids, context=None):
        account_expense_gross_salaries_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.tosa.payroll','account_expense_gross_salaries_id')
        return {'account_expense_gross_salaries_id' : account_expense_gross_salaries_id }
        
