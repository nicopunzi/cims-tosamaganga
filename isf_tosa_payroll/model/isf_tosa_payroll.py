from osv import osv, fields
import logging
import datetime

_logger = logging.getLogger(__name__)
_debug=True

class isf_tosa_payroll(osv.Model):
    _name = 'isf.tosa.payroll'
    
    def _get_default_journal_id(self, cr, uid, context=None):
       return self.pool.get('ir.values').get_default(cr, uid, 'isf.tosa.payroll','advances_journal_id')
   
    def _get_default_advances_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.tosa.payroll','advances_account_id')
    
    def _get_default_period_id(self, cr, uid, context=None):
        period_obj = self.pool.get('account.period')
        date = datetime.datetime.now()
        period_ids = period_obj.find(cr, uid, date, context=context)
        period = period_ids[0]
        
        return period
    
    def _get_account_expense_gross_salaries(self, cr, uid, context=None):
        if context is None:
            context = {}
        ir_values = self.pool.get('ir.values')
        expense_ids = ir_values.get_default(cr, uid, 'isf.tosa.payroll', 'account_expense_gross_salaries_id')
        
        return expense_ids
    
    def _get_account_expense_gross_salaries_text(self, cr, uid, context=None):
        if context is None:
            context = {}
        
        account_id = self._get_account_expense_gross_salaries(cr, uid, context=context)
        if _debug:
            _logger.debug('account_id: %s', account_id)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def action_confirm(self, cr, uid, ids, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','ISF Payroll')])
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids, context)
        self.write(cr, uid, ids, {'sequence_id' : new_seq, 'state' : 'confirmed'}, context=context)
        
        return True
    
    def action_compute(self, cr, uid, ids, context=None):
        
        self.write(cr, uid, ids, {'state' : 'computed'}, context=context)
        return True
    
    def action_pay(self, cr, uid, ids, context=None):
        return True
    
    def _check_description(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        description = data.name
        if not description:
            return False
        
        return True
    
    def calculate_advances_on_payroll(self, cr, uid, ids, period, context=None):
        if _debug:
            _logger.debug("############### CHANGED PERIOD ###############")
            _logger.debug('period_id: %s', period['period_id'])
            _logger.debug('ids: %s', ids)
        if not period:
            return False
        
        move_line_pool = self.pool.get('account.move.line')
        advance_line_pool = self.pool.get('isf.tosa.payroll.advances.lines')
        
        #remove previous added lines
        advance_lines_ids = advance_line_pool.search(cr, uid, [('isf_payroll_id','in', ids)])
        advance_line_pool.unlink(cr, uid, advance_lines_ids)
        
        #add new lines and calculate balance
        _balance_advances = 0.0
        _advance_journal_id = self._get_default_journal_id(cr, uid)
        _advance_account_id = self._get_default_advances_account_id(cr, uid)
        move_line_ids = move_line_pool.search(cr, uid, [('period_id','=',period['period_id']),('journal_id','=',_advance_journal_id),('account_id','=',_advance_account_id)])
        move_line_obj = move_line_pool.browse(cr, uid, move_line_ids, context)
        if move_line_obj:
            for move_line in move_line_obj:
                if _debug:
                    _logger.debug(move_line.id)
            advance_line_pool.create(cr, uid, {
                'isf_payroll_id' : ids[0],
                'date' : move_line.date,
                'name' : move_line.name,
                'amount' : move_line.debit,                                         
            })
            _balance_advances += move_line.debit
        
        if _debug:
            _logger.debug('Balance: %s', _balance_advances)
        
        #save balance
        self.write(cr, uid, ids, {'balance_advances' : _balance_advances,})
                
        return True
    
    def onchange_computing_fields(self,cr,uid,ids,context=None):
        balance_computing = 0.0
        
        result = {'value':{} }
        result['value'].update({
            'balance_computing' : balance_computing,
        })
        
        return result
    
    _columns = {
        #Main Section
        'sequence_id' : fields.char('ID',char=64),
        'name' : fields.text('Description', readonly=False, required=True),
        'period_id' : fields.many2one('account.period', 'Period', required=True),
        'state' : fields.selection([('draft','Draft'), ('confirmed','Confirmed'), ('computed','Computed'), ('paid','Paid')], 'State', required=True, readonly=True),
        #Section for advances on payroll
        'journal_entries_advances_ids' : fields.one2many('isf.tosa.payroll.advances.lines','isf_payroll_id','Advances on Payroll', required=False,readonly=True),
        'balance_advances' : fields.float('Balance Advances', readonly=True, required=False),
        #Section for payroll computing
        'account_expense_gross_salaries' : fields.many2one('account.account','Gross Amount', required=True),
        'debit_expense_gross_salaries' : fields.float('Debit Expense Gross Salaries', required=False),
        'credit_expense_gross_salaries' : fields.float('Credit Expense Gross Salaries', required=False),
        'account_expense_gross_salaries_text' : fields.char('Gross Amount',size=64,readonly=True),
#         'account_liability_net_salaries_id' : fields.one2one(''),
#         'amount_liability_net_salaries' : fields.float('Liability Net Salaries', required=False),
        'balance_computing' : fields.float('Balance Computing', required=False),
        #Section for payroll payment
        'balance_payment' : fields.float('Balance Payment', required=False),
    }
    
    _defaults = {
        'period_id': _get_default_period_id,
        'state' : 'draft',
        'balance_advances' : 0.0,
        'balance_computing' : 0.0,
        'balance_payment' : 0.0,
        'account_expense_gross_salaries' : _get_account_expense_gross_salaries,
        'debit_expense_gross_salaries' : 0.0,
        'credit_expense_gross_salaries' : 0.0,
        'account_expense_gross_salaries_text' : _get_account_expense_gross_salaries_text,
    }
    
    _sql_constraints = [('name_uniq','unique(name)','Description must be unique!'),
                        ('period_uniq','unique(period_id)','Only one payroll per month!')]
    
    _constraints = [
         (_check_description, 'Please insert a description ',['name']),
    ]
    
    
    
#     def create(self, cr, uid, vals, context=None):
#         obj_sequence = self.pool.get('ir.sequence')
#         seq_ids = obj_sequence.search(cr, uid, [('name','=','ISF Payroll')])
#         new_seq = obj_sequence.next_by_id(cr, uid, seq_ids, context)
#         vals['sequence_id'] = new_seq
#         res_id = super(isf_tosa_payroll, self).create(cr, uid, vals, context=context)
#         return res_id

isf_tosa_payroll()
    
    
class isf_tosa_payroll_advances_lines(osv.osv):
    _name = 'isf.tosa.payroll.advances.lines'
    _columns = {
        'isf_payroll_id' : fields.many2one('isf.tosa.payroll','isf_payroll_id',select=True, required=True, ondelete='cascade'),
        'date' : fields.date('Date'),
        'name' : fields.char('Name', size=32),
        'amount' : fields.float('Amount'),
    }
  
isf_tosa_payroll_advances_lines()
