import time
import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from os.path import join as opj

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging

_logger = logging.getLogger(__name__)
_debug=False

class isf_payrol_config_settings(osv.osv_memory):
    _name = 'isf.tosa.payroll.config.settings'
    _inherit = 'res.config.settings'

    _columns = {
        # Common fields
        'cash_bank_account' : fields.many2many('account.account','isf_cc_account_cash_bank','name','id','Cash/Bank',required=True),
        # Salary Advance On Payroll
        'adv_journal_id' : fields.many2one('account.journal','Journal',required=True),
        'adv_expense_account' : fields.many2many('account.account','isf_sapc_account_expense','name','id','Expense',required=True),
        'adv_help' : fields.text('Help', size=512, required=False),
        # Payroll Computing
        'account_expense_gross' : fields.many2one('account.account','Wage & Salaries Expense Account',required=True),
        'account_liability_net' : fields.many2one('account.account','Net Payable Account',required=True),
        'account_liability_nssf' : fields.many2one('account.account','NSSF Payable Account',required=True),
        'account_liability_paye' : fields.many2one('account.account','PAYE Payable Account',required=True),
        'account_liability_tughe' : fields.many2one('account.account','TUGHE Payable Account',required=True),
        'account_liability_nhif' : fields.many2one('account.account','NHIF Payable Account',required=True),
        'account_liability_saccos' : fields.many2one('account.account','SACCOS Payable Account',required=False),
        'account_liability_ustawi' : fields.many2one('account.account','USTAWI Payable Account',required=False),
        'account_receivable_advances' : fields.many2one('account.account','Receivable Advances Account',required=False),
        'account_expense_nssf_employer' : fields.many2one('account.account','NSSF Employer Expense',required=True),
        'pay_comp_journal_id' : fields.many2one('account.journal','Journal',required=True),
        'pay_comp_analytic_account_ids' : fields.many2many('account.analytic.account','isf_pcc_analytic','name','id','Analytic Account',required=True),
        'pay_comp_help' : fields.text('Help', size=512, required=False),
        # Payroll Payments
        'payment_journal_id' : fields.many2one('account.journal','Journal',required=True),
        'payment_liability_account' : fields.many2many('account.account','isf_ppc_account_expense','name','id','Liability',required=True),
        'payment_help' : fields.text('Help', size=512, required=False),
    }
    
    def _get_pay_comp_help(self, cr, uid, context=None):
        return 'Register gross salaries and their liabilities. Advances should be specified only if addebited before.'

    def _get_payment_help(self, cr, uid, context=None):
        return 'Register net salaries and taxes payment'

    def _get_adv_help(self, cr, uid, context=None):
        return 'Register salary advances on payroll.'
    
    _defaults = {
        'pay_comp_help' : _get_pay_comp_help,
        'payment_help' : _get_payment_help,
        'adv_help' : _get_adv_help,
    }

    # Set Method
    def set_cash_bank_account(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        
        account_ids = []
        for account in config.cash_bank_account:
            account_ids.append(account.id)
            
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'cash_bank_account',account_ids)

    def set_account_expense_gross(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_expense_gross',config.account_expense_gross.id)

    def set_account_liability_net(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_net',config.account_liability_net.id)
        
    def set_account_liability_nssf(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_nssf',config.account_liability_nssf.id)
        
    def set_account_liability_paye(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_paye',config.account_liability_paye.id)
        
    def set_account_liability_tughe(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_tughe',config.account_liability_tughe.id)   
        
    def set_account_liability_nhif(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_nhif',config.account_liability_nhif.id)   
        
    def set_account_liability_saccos(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_saccos',config.account_liability_saccos.id)   
        
    def set_account_liability_ustawi(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_ustawi',config.account_liability_ustawi.id)   
        
    def set_account_receivable_advances(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_receivable_advances',config.account_receivable_advances.id)

    def set_account_expense_nssf_employer(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_expense_nssf_employer',config.account_expense_nssf_employer.id)

    def set_pay_comp_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_journal_id',config.pay_comp_journal_id.id)

    def set_pay_comp_analytic_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        
        account_ids = []
        for account in config.pay_comp_analytic_account_ids:
            account_ids.append(account.id)
            
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_analytic_account_ids',account_ids)

    def set_pay_comp_help(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_help',config.pay_comp_help)

    def set_payment_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'payment_journal_id',config.payment_journal_id.id)

    def set_payment_liability_account(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        
        account_ids = []
        for account in config.payment_liability_account:
            account_ids.append(account.id)
            
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'payment_liability_account',account_ids)

    def set_payment_help(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'payment_help',config.payment_help)

    def set_adv_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'adv_journal_id',config.adv_journal_id.id)

    
    def set_adv_expense_account(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        
        account_ids = []
        for account in config.adv_expense_account:
            account_ids.append(account.id)
            
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'adv_expense_account',account_ids)

    def set_adv_help(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll.config.settings', 'adv_help',config.adv_help)
       
    
    #Get Method
    def get_default_cash_bank_account(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'cash_bank_account')
        
        return {'cash_bank_account' : account_ids}

    def get_default_journal_id(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        journal_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'journal_id')

        return {'journal_id' : journal_id}

    def get_default_pay_comp_journal_id(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        journal_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_journal_id')

        return {'pay_comp_journal_id' : journal_id}

    def get_default_account_expense_gross(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_expense_gross')

        return {'account_expense_gross' : account_id}

    def get_default_account_liability_net(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_net')

        return {'account_liability_net' : account_id}
    
    def get_default_account_liability_nssf(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_nssf')

        return {'account_liability_nssf' : account_id}
    
    def get_default_account_liability_paye(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_paye')

        return {'account_liability_paye' : account_id}
    
    def get_default_account_liability_tughe(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_tughe')

        return {'account_liability_tughe' : account_id}
    
    def get_default_account_liability_nhif(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_nhif')

        return {'account_liability_nhif' : account_id}
    
    def get_default_account_liability_saccos(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_saccos')

        return {'account_liability_saccos' : account_id}
    
    def get_default_account_liability_ustawi(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_ustawi')

        return {'account_liability_ustawi' : account_id}
    
    def get_default_account_receivable_advances(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_receivable_advances')

        return {'account_receivable_advances' : account_id}

    def get_default_account_expense_nssf_employer(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_expense_nssf_employer')

        return {'account_expense_nssf_employer' : account_id}
    
    def get_default_pay_comp_analytic_account_ids(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_analytic_account_ids')

        return {'pay_comp_analytic_account_ids' : account_ids}

    def get_default_pay_comp_help(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        help_str = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_help')
        
        return {'ay_comp_help' : help_str}

    def get_default_payment_journal_id(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        journal_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'payment_journal_id')

        return {'payment_journal_id' : journal_id}

    def get_default_payment_liability_account(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'payment_liability_account')

        return {'payment_liability_account' : account_ids}

    def get_default_payment_help(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        help_str = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'payment_help')

        return {'payment_help' : help_str}

    def get_default_adv_journal_id(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        journal_id = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'adv_journal_id')

        return {'adv_journal_id' : journal_id}

    
    def get_default_adv_expense_account(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'adv_expense_account')

        return {'adv_expense_account' : account_ids}

    def get_default_adv_help(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        help_str = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'adv_help')

        return {'adv_help' : help_str}


        
        


