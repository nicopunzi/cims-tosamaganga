from openerp.osv import osv
import logging

_logger = logging.getLogger(__name__)
_debug=False

class isf_tosa_payroll_autoconfig(osv.osv_memory):
    _name = "isf.tosa.payroll.autoconfig"

    def _set_isf_tosa_payroll(self, cr, uid):
        _advances_journal_code = 'SAPJ'
        _advances_account = 1400003
        _account_expense_gross_salaries_id = 5400001
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.tosa.payroll', 'advances_journal_id',self._get_account_journal_id_by_code(cr, uid, _advances_journal_code))
        ir_values.set_default(cr, uid, 'isf.tosa.payroll', 'advances_account_id',self._get_account_id(cr, uid, _advances_account))
        ir_values.set_default(cr, uid, 'isf.tosa.payroll', 'account_expense_gross_salaries_id',self._get_account_id(cr, uid, _account_expense_gross_salaries_id))
    
    
    def _get_account_id(self, cr, uid, account_number):
        account_obj = self.pool.get('account.account')
        account_id = account_obj.search(cr, uid, [('code','=',account_number)])
        if account_id:
            return account_id[0]
    
  
    def _get_account_journal_id_by_code(self, cr, uid, journal_code):
        journal_obj = self.pool.get('account.journal')
        journal_id = journal_obj.search(cr, uid, [('code','=',journal_code)])
        if journal_id:
            return journal_id[0]
    
    
    def _auto_config(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
        
        if _debug:
            _logger.info("########### ISF TOSA AUTO-CONFIG ###########")
        
        self._set_isf_tosa_payroll(cr, uid)

            
isf_tosa_payroll_autoconfig()

    