# -*- coding: utf-8 -*-

{
	'name': 'ISF Tosa Payroll',
	'version': '0.1',
	'category': 'Tools',
	'description': """

ISF Tosa Payroll 
========
Add the following functions. All functionality can be configured in Settings/Configuration

* Salary Advances On Payroll
* Advance Payment Refund
* Payroll Computing
* Payroll Payment

""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': [
            'base',
            'isf_cims_module',
    ],
	'data': [
        # Views
        'views/isf_tosa_payroll_view.xml',
        'views/res_config_view.xml',
        # Workflow
        #'workflow/isf_tosa_payroll_workflow.xml',
        # Data
        'data/sequence/sequence.xml',
        'isf_tosa_payroll_autoconfig.xml',
    ],
	'demo': [],
	'installable' : True,
}

