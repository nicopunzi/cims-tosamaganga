from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

_logger = logging.getLogger(__name__)
_debug=True
    

class isf_tosa_payroll_computing(osv.osv_memory):
    _name = 'isf.tosa.payroll.computing'
    
    def _get_account_expense_gross(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_expense_gross')
    
    def _get_account_expense_gross_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_expense_gross(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_liability_net(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_net')
                
        
    def _get_account_liability_net_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_net(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_liability_nssf(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_nssf')
        
    def _get_account_liability_nssf_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_nssf(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_liability_paye(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_paye')
        
    def _get_account_liability_paye_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_paye(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_liability_tughe(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_tughe')
        
    def _get_account_liability_tughe_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_tughe(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_liability_nhif(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_nhif')
        
    def _get_account_liability_nhif_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_nhif(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_liability_saccos(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_saccos')
        
    def _get_account_liability_saccos_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_saccos(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_liability_ustawi(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_liability_ustawi')
        
    def _get_account_liability_ustawi_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_ustawi(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
        
    def _get_account_receivable_advances(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_receivable_advances')
        
    def _get_account_receivable_advances_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_receivable_advances(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_expense_nssf_employer(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'account_expense_nssf_employer')
        
    def _get_account_expense_nssf_employer_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_expense_nssf_employer(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
        
    def _get_default_journal_id(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_journal_id')
            
    def _get_analytic_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        
        ir_values = self.pool.get('ir.values')
        aa_ids = ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_analytic_account_ids')

        if aa_ids:
            for aa_id in aa_ids:
                aa_obj = self.pool.get('account.analytic.account')
                aa_ids = aa_obj.search(cr, uid, [('id','=',aa_id)])
                if aa_ids:
                    aa = aa_obj.browse(cr, uid, aa_ids)[0]
                    result.append((aa.id, aa.name))        
                
        return result
    
    def _get_help(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.tosa.payroll.config.settings', 'pay_comp_help')
    
    _columns = {
        'journal_id' : fields.many2one('account.journal','Journal', required=True),
        'ref' : fields.char('Reference', size=64,required=True),
        'name' : fields.char('Description', size=64, required=True),
        'date' : fields.date('Date', required=True),
        'account_expense_gross' : fields.many2one('account.account','Wage & Salaries Expense Account', required=True),
        'account_liability_net' : fields.many2one('account.account','Net Payable Account', required=True),
        'account_liability_nssf' : fields.many2one('account.account','NSSF Payable Account', required=True),
        'account_liability_paye' : fields.many2one('account.account','PAYE Payable Account', required=True),
        'account_liability_tughe' : fields.many2one('account.account','TUGHE Payable Account', required=True),
        'account_liability_nhif' : fields.many2one('account.account','NHIF Payable Account', required=True),
        'account_liability_saccos' : fields.many2one('account.account','SACCOS Payable Account', required=False),
        'account_liability_ustawi' : fields.many2one('account.account','USTAWI Payable Account', required=False),
        'account_receivable_advances' : fields.many2one('account.account','Receivable Advances Account', required=False),
        'account_expense_nssf_employer' : fields.many2one('account.account','NSSF Employer Expense', required=True),
        'analytic_account' : fields.selection(_get_analytic_accounts,'Analytic account', required=True),
        'amount_expense_gross' : fields.float('Wage & Salaries Amount', required=True),
        'amount_liability_net' : fields.float('Net Amount', required=True),
        'amount_liability_nssf' : fields.float('NSSF Amount', required=True),
        'amount_liability_paye' : fields.float('PAYE Amount', required=True),
        'amount_liability_tughe' : fields.float('TUGHE Amount', required=True),
        'amount_liability_nhif' : fields.float('NHIF Amount', required=True),
        'amount_liability_saccos' : fields.float('SACCOS Amount', required=False),
        'amount_liability_ustawi' : fields.float('USTAWI Amount', required=False),
        'amount_receivable_advances' : fields.float('Receivable Advances Amount', required=False),
        'amount_expense_nssf_employer' : fields.float('NSSF Employer', required=True),
        'currency' : fields.many2one('res.currency','Currency', required=True,help='Currency of the operation'),
        'balance' : fields.float('Balance', size=32,help='Balance must be zero if data are correct'),
        'account_expense_gross_text' : fields.char('Wage & Salaries Amount',size=64,readonly=True),
        'account_liability_net_text' : fields.char('Net Amount',size=64,readonly=True),
        'account_liability_nssf_text' : fields.char('NSSF Amount',size=64,readonly=True),
        'account_liability_paye_text' : fields.char('PAYE Amount',size=64,readonly=True),
        'account_liability_tughe_text' : fields.char('TUGHE Amount',size=64,readonly=True),
        'account_liability_nhif_text' : fields.char('NHIF Amount',size=64,readonly=True),
        'account_liability_saccos_text' : fields.char('SACCOS Amount',size=64,readonly=True),
        'account_liability_ustawi_text' : fields.char('USTAWI Amount',size=64,readonly=True),
        'account_receivable_advances_text' : fields.char('Receivable Advances Amount',size=64,readonly=True),
        'account_expense_nssf_employer_text' : fields.char('NSSF Employer Amount',size=64,readonly=True),
        'help' : fields.text('Help', size=512),
    }
    
    _defaults = {
        'journal_id' : _get_default_journal_id,
        'date' : fields.date.context_today,
        'account_expense_gross' : _get_account_expense_gross,
        'account_liability_net' : _get_account_liability_net,
        'account_liability_nssf' : _get_account_liability_nssf,
        'account_liability_paye' : _get_account_liability_paye,
        'account_liability_tughe' : _get_account_liability_tughe,
        'account_liability_nhif' : _get_account_liability_nhif,
        'account_liability_saccos' : _get_account_liability_saccos,
        'account_liability_ustawi' : _get_account_liability_ustawi,
        'account_receivable_advances' : _get_account_receivable_advances,
        'account_expense_nssf_employer' : _get_account_expense_nssf_employer,
        
        'account_expense_gross_text' : _get_account_expense_gross_text,
        'account_liability_net_text' : _get_account_liability_net_text,
        'account_liability_nssf_text' : _get_account_liability_nssf_text,
        'account_liability_paye_text' : _get_account_liability_paye_text,
        'account_liability_tughe_text' : _get_account_liability_tughe_text,
        'account_liability_nhif_text' : _get_account_liability_nhif_text,
        'account_liability_saccos_text' : _get_account_liability_saccos_text,
        'account_liability_ustawi_text' : _get_account_liability_ustawi_text,
        'account_receivable_advances_text' : _get_account_receivable_advances_text,
        'account_expense_nssf_employer_text' : _get_account_expense_nssf_employer_text,
        'amount_receivable_advances' : 0.0,
        'balance' : 0.0,
        'help' : _get_help,
         
    }
    
    def _check_amount_expense_gross(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
          
        if obj.amount_expense_gross <= 0.0 :
            return False
        return True
        
    def _check_amount_liability_net(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
            
        if obj.amount_liability_net <= 0.0 :
            return False
        return True
        
    def _check_amount_liability_nssf(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
         
        if obj.amount_liability_nssf >= 0.0 :
            return True
        return False
        
    def _check_amount_receivable_advances(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
         
        if obj.amount_receivable_advances >= 0.0 :
            return True
        return False
        
    def _check_balanced(self, cr, uid, ids ,context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        
        
        balance = obj.amount_expense_gross  - (obj.amount_liability_net + obj.amount_liability_nssf + obj.amount_liability_paye + obj.amount_liability_tughe + obj.amount_liability_nhif + obj.amount_liability_saccos + obj.amount_liability_ustawi + obj.amount_receivable_advances + obj.amount_expense_nssf_employer)
        if _debug:
            _logger.debug('###### Balance : %f', balance)
        if balance == 0.0:
            return True
            
        return False

    _constraints = [
         (_check_amount_expense_gross, 'Gross mount must be positive ( > 0.0)',['']),
         (_check_amount_liability_net, 'Net mount must be positive ( > 0.0)',['']),
         (_check_amount_liability_nssf, 'Deduction amount must be positive ( > 0.0)',['']),
         (_check_amount_receivable_advances, 'Advances amount must be positive ( > 0.0)',['']),
         (_check_balanced, 'Entry must be balanced, check your data',['']),
    ]

    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
	
	
    
    def compute_payroll(self, cr, uid, ids, context=None):
        if context is None:
            context = {}    
        
        data = self.browse(cr, uid, ids, context=context)[0]

        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
		
        move = move_pool.account_move_prepare(cr, uid, data.journal_id.id, data.date, ref=data.ref, context=context)	
        move_id = move_pool.create(cr, uid, move, context=context)
		
		
        if _debug:
            _logger.debug('move_id : %s', move_id)

        company_currency_id = self._get_company_currency_id(cr, uid, context=context)
 
        currency = False
        amount_expense_gross_currency = False
        amount_liability_net_currency = False
        amount_liability_nssf_currency = False
        amount_liability_paye_currency = False
        amount_liability_tughe_currency = False
        amount_liability_nhif_currency = False
        amount_liability_saccos_currency = False
        amount_liability_ustawi_currency = False
        amount_receivable_advances_currency = False
        amount_expense_nssf_employer_currency = False
        
        amount_expense_gross = data.amount_expense_gross
        amount_liability_net = data.amount_liability_net
        amount_liability_nssf = data.amount_liability_nssf
        amount_liability_paye = data.amount_liability_paye
        amount_liability_tughe = data.amount_liability_tughe
        amount_liability_nhif = data.amount_liability_nhif
        amount_liability_saccos = data.amount_liability_saccos
        amount_liability_ustawi = data.amount_liability_ustawi
        amount_receivable_advances = data.amount_receivable_advances
        amount_expense_nssf_employer = data.amount_expense_nssf_employer
        
        if data.currency.id != company_currency_id:
            c_pool = self.pool.get('res.currency')
            currency = data.currency.id
            amount_expense_gross = c_pool.compute(cr, uid,data.currency.id, company_currency_id,  data.amount_expense_gross, context=context)
            amount_liability_net = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_net, context=context)
            amount_liability_nssf = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_nssf, context=context)
            amount_liability_paye = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_paye, context=context)
            amount_liability_tughe = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_tughe, context=context)
            amount_liability_nhif = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_nhif, context=context)
            amount_liability_saccos = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_saccos, context=context)
            amount_liability_ustawi = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_ustawi, context=context)
            amount_receivable_advances = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_receivable_advances, context=context)
            amount_expense_nssf_employer = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_expense_nssf_employer, context=context)
            
            amount_expense_gross_currency = data.amount_expense_gross
            amount_liability_net_currency = -1 * data.amount_liability_net
            amount_liability_nssf_currency = -1 * data.amount_liability_nssf
            amount_liability_paye_currency = -1 * data.amount_liability_paye
            amount_liability_tughe_currency = -1 * data.amount_liability_tughe
            amount_liability_nhif_currency = -1 * data.amount_liability_nhif
            amount_liability_saccos_currency = -1 * data.amount_liability_saccos
            amount_liability_ustawi_currency = -1 * data.amount_liability_ustawi
            amount_receivable_advances_currency = -1 * data.amount_receivable_advances
            amount_expense_nssf_employer_currency = -1 * data.amount_expense_nssf_employer
            
        move_line = {
            'analytic_account_id': data.analytic_account or False,
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency,
            'credit': 0.0,
            'debit': amount_expense_gross,
            'date_maturity' : False,
            'amount_currency': amount_expense_gross_currency,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.account_expense_gross),
            'state' : 'valid'
        }
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
			
        move_line = {
            'analytic_account_id': False,
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency,
            'credit': amount_liability_net,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': amount_liability_net_currency,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.account_liability_net),
            'state' : 'valid'
        }
	
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        if data.amount_liability_nssf > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_liability_nssf,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_liability_nssf_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_liability_nssf),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
            
        if data.amount_liability_paye > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_liability_paye,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_liability_paye_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_liability_paye),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
            
        if data.amount_liability_tughe > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_liability_tughe,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_liability_tughe_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_liability_tughe),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
            
        if data.amount_liability_nhif > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_liability_nhif,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_liability_nhif_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_liability_nhif),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
            
        if data.amount_liability_saccos > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_liability_saccos,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_liability_saccos_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_liability_saccos),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
            
        if data.amount_liability_ustawi > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_liability_ustawi,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_liability_ustawi_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_liability_ustawi),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        if data.amount_receivable_advances > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_receivable_advances,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_receivable_advances_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_receivable_advances),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
            
        if data.amount_expense_nssf_employer > 0.0:
            move_line = {
                'analytic_account_id': data.analytic_account or False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_expense_nssf_employer,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_expense_nssf_employer_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_expense_nssf_employer),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        return {'type': 'ir.actions.act_window_close'}
    
    def _get_analytic_journal_by_name(self, cr, uid, name):
        analytic_journal_pool = self.pool.get('account.analytic.journal')
        analytic_journal_ids = analytic_journal_pool.search(cr, uid, [('name','=',name)])
        return analytic_journal_ids[0] if len(analytic_journal_ids) else None
            
    def _create_default_journal(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
            
        journal_pool = self.pool.get('account.journal')
        journal_ids = journal_pool.search(cr, uid, [('code','=','PRCJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Payroll Computing Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Payroll Computing Sequence',
                    'prefix' : 'PRC/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            journal = {
                'name' : 'Payroll Computing Journal',
                'code' : 'PRCJ',
                'type' : 'general',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'General'),
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        return True
        
    def onchange_amount(self,cr,uid,ids,amount_expense_gross,amount_liability_net,amount_liability_nssf,amount_liability_paye,amount_liability_tughe,amount_liability_nhif,amount_liability_saccos,amount_liability_ustawi,amount_receivable_advances,amount_expense_nssf_employer,context=None):
        if _debug:
            _logger.debug('amount_expense_gross : %d',amount_expense_gross)
            _logger.debug('amount_liability_net : %d',amount_expense_gross)
            _logger.debug('amount_liability_nssf : %d',amount_expense_gross)
            _logger.debug('amount_liability_paye : %d',amount_expense_gross)
            _logger.debug('amount_liability_tughe : %d',amount_expense_gross)
            _logger.debug('amount_liability_nhif : %d',amount_expense_gross)
            _logger.debug('amount_liability_saccos : %d',amount_expense_gross)
            _logger.debug('amount_liability_ustawi : %d',amount_expense_gross)
            _logger.debug('amount_receivable_advances : %d',amount_expense_gross)
            _logger.debug('amount_expense_nssf_employer : %d',amount_expense_gross)
        
        balance = amount_expense_gross  - (amount_liability_net + amount_liability_nssf + amount_liability_paye + amount_liability_tughe + amount_liability_nhif + amount_liability_saccos + amount_liability_ustawi + amount_receivable_advances + amount_expense_nssf_employer)
        
        result = {'value':{} }
        result['value'].update({
            'balance' : balance,
        })
        
        return result
        
    def _redirect(self, cr, uid,  context=None):
        return {
            'name':_('Test'),
            'view_mode': 'list',
            'view_id': False,
            #'views': [(resource_id,'form')],
            'view_type': 'list, tree, form',
            'view_id' : 328, # id of the object to which to redirected
            'res_model': 'account.move', # object name
            'type': 'ir.actions.act_window',
            #'target': 'inline', # if you want to open the form in new tab
        }
        
isf_tosa_payroll_computing()
