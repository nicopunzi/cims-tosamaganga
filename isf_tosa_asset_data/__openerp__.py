# -*- encoding: utf-8 -*-

{
    'name': 'Tosamaganga - Asset Data',
    'version': '0.9',
    'author': 'Alessandro Domanico <alessandro.domanico@informaticisenzafrontiere.org>',
    'description': """
Tosamaganga Hospital Asset Data (first import)
==============================================

The module imports:

* 12 categories and asset views
* 1763 assets

It requires following modules installed:

* account_asset_management
* isf_tosa_asset

    """,
    'license': 'AGPL-3',
    'category': 'Localization',
    'data': ['data/account.asset.category.csv',
             'data/account.asset.asset.csv',
             ],
    'demo': [],
    'installable': True,
    'auto_install': False
}
