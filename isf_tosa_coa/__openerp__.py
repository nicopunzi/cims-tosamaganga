# -*- encoding: utf-8 -*-

{
    'name': 'Tosamaganga - Accounting',
    'version': '0.8',
    'depends': ['account_accountant','account_check_writing'],
    'author': 'Alessandro Domanico for ISF',
    'description': """
Tosamaganga Hospital Chart of Accounts
================================================

Tosamaganga Hospital accounting chart and localization.
    """,
    'license': 'AGPL-3',
    'category': 'Localization/Account Charts',
    'data': [
		#'security/ir.model.access.csv', <-- file present just as example
        'data/account.account.type.csv',
        'data/account.account.template.csv',
		'data/account.tax.code.template.csv',
		'account_chart.xml',
		'base_data.xml',
		'currency_data.xml',
		'data/account.tax.template.csv',
		'data/account.fiscal.position.template.csv',
        'l10n_chart_en_tosa_generic.xml',
        ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}