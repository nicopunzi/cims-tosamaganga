from openerp.osv import osv
import logging

logger = logging.getLogger()

class isf_tosa_products(osv.osv):
    _name = "isf.tosa.products"
    _auto = False

    def init(self, cr):
        logger.info("*** isf_tosa_products.init CALLED! ***")
        try:
            self.deleteDefaultProductService(cr)
            self.setDefaultAnalyticJournalS(cr)
            self.setDefaultDecimalPrecision(cr)
        except Exception, ex:
            logger.exception(ex)

    def deleteDefaultProductService(self, cr):
        res = cr.execute("DELETE FROM product_product WHERE id = 1")
        cr.commit()
        return True
    
    def setDefaultAnalyticJournalS(self, cr):
        res = cr.execute("UPDATE account_journal SET analytic_journal_id = 2 WHERE code = 'EXJ'")
        res = cr.execute("UPDATE account_journal SET analytic_journal_id = 2 WHERE code = 'ECNJ'")
        res = cr.execute("UPDATE account_journal SET analytic_journal_id = 3 WHERE code = 'MISC'")
        res = cr.execute("UPDATE account_journal SET analytic_journal_id = 3 WHERE code = 'STJ'")
        cr.commit()
        return True
    
    def setDefaultDecimalPrecision(self, cr):
        res = cr.execute("UPDATE decimal_precision SET digits = 0 WHERE name = 'Product Unit of Measure'")
    
isf_tosa_products()
