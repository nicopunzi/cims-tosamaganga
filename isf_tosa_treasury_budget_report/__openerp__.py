# -*- coding: utf-8 -*-

{
	'name': 'ISF Tosamaganga Treasury Budget Report',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF Tosamaganga Treasury Budget Report 
======================================

Tosamaganga Treasury Budget Report schema
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['isf_cims_module','isf_treasury_budget_report'],
	'data': [
		'data/isf.treasury.report.csv',
        'isf_tosa_treasury_report.xml',
    ],
	'demo': [],
	'installable' : True,
}

