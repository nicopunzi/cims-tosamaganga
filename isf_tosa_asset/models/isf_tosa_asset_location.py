from openerp.osv import fields, orm, osv
import logging
import time
from openerp import tools

_logger = logging.getLogger(__name__)
_debug = False

class isf_tosa_asset_location(orm.Model):
    _name = 'isf.tosa.asset.location'
    _description = 'Asset Location'
    _order = 'name'
    
    _columns = {
        'parent_location_id' : fields.many2one('isf.tosa.asset.location','Parent Location'),
        'name' : fields.char('Name', required=True),
    }
    
    _sql_constraints = [(
        'name', 'unique(name)', 'Location already exists')]
    
    def _insertUpdateLocations(self, cr, uid, ids=None, context=None):
        context = context or {}
        module_update_obj = self.pool.get('isf.module.update')
        updated = True
        if not module_update_obj.is_updated(cr, uid, __name__, _debug, context):
            if _debug:
                _logger.debug('==> Updating...')
            locations = cr.execute("SELECT DISTINCT(location) FROM account_asset_asset")
            locations = cr.fetchall()
            for location in locations:
                if _debug:
                    _logger.debug('==> location : %s', location[0])
                if location[0]:
                    id = 0
                    try:
                        cr.execute("INSERT INTO isf_tosa_asset_location(create_uid, create_date, write_date, write_uid, name) VALUES (1, (now() at time zone 'UTC'), (now() at time zone 'UTC'), 1, %s) RETURNING id", (location))
                        id = cr.fetchone()[0]
                        _logger.debug('==> location_id : %s', id)
                        cr.execute("UPDATE account_asset_asset SET location_id = %s WHERE location = %s", (id, location[0]))
                    except Exception, ex:
                        _logger.exception('==> handled Exception : %s', ex)
                        updated = False
            cr.commit()
            if updated:
                module_update_obj.module_updated(cr, uid, __name__, _debug, context)
        else:
            if _debug:
                _logger.debug('==> Already updated!')
        return True
    
isf_tosa_asset_location()