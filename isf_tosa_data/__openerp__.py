# -*- encoding: utf-8 -*-

{
    'name': 'Tosamaganga - Accounting Data',
    'version': '0.9',
    'depends': ['isf_tosa_coa'],
    'author': 'Alessandro Domanico for ISF',
    'description': """
Tosamaganga Hospital Chart of Accounts Data
================================================

Tosamaganga Hospital Analytic Accounts, Banks and Partners.
    """,
    'license': 'AGPL-3',
    'category': 'Localization',
    'data': [
		#'security/ir.model.access.csv', <-- file present just as example
		'data/account.analytic.journal.csv',
        'data/account.analytic.account.csv',
        'data/account.journal.csv',
        'data/res.bank.csv',
        'data/res.partner.bank.csv',
        'data/res.partner.csv',
        'data/ir.property.csv',
		'base_data.xml',
        'data/res.company.csv',
        ],
    'demo': [],
    'installable': True,
    'auto_install': False
}
