from openerp.osv import osv
import logging

_logger = logging.getLogger(__name__)
_debug = False

class isf_tosa_data(osv.osv):
    _name = "isf.tosa.data"
    _auto = False

    def init(self, cr):
        if _debug:
            _logger.info("*** isf_tosa_data.init CALLED! ***")
        
        try:
            self.deleteDefaultBankAndCashAccount(cr)
        except Exception, ex:
            _logger.exception(ex)
        
        if _debug:
            _logger.info("########### CREATING ACCOUNT KEYS")
        ids_c = cr.execute("select id, code from account_account")
        ids = cr.fetchall()
        for acdata in ids:
            aid, acode = acdata
            name_id = acode
            model = "account.account"
            module = "isf_tosa_data"
            cr.execute('select * from ir_model_data where name=%s and module=%s', (name_id, module))
            if not cr.rowcount:
                cr.execute("INSERT INTO ir_model_data (name,date_init,date_update,module,model,res_id) VALUES (%s, (now() at time zone 'UTC'), (now() at time zone 'UTC'), %s, %s, %s)", \
                    (name_id, module, model, aid)
                )
        cr.commit()

    def deleteDefaultBankAndCashAccount(self, cr):
        res = cr.execute("select id from account_account where code IN ('1500010', '1500011')")
        account_ids = cr.fetchall()
        if not account_ids:
            return False
        res = cr.execute("DELETE FROM account_journal where default_debit_account_id IN (%s)" % ','.join([str(a[0]) for a in account_ids]))
        res = cr.execute("DELETE FROM account_account where code IN ('1500010', '1500011')")
        cr.commit()
        return True
        
    def _onchange_footer(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
        
        if _debug:
            _logger.info("########### GETTING COMPANY POOL")
        company_pool = self.pool.get('res.company')
        company_ids = company_pool.search(cr, uid, [],limit=1)
        company_obj = company_pool.browse(cr, uid, company_ids, context=context)
        
        for company in company_obj:
            
            custom_footer = company.custom_footer
            phone = company.phone
            fax = company.fax
            email = company.email
            website = company.website
            vat = company.vat
            company_registry = company.company_registry
            bank_ids = company.bank_ids
            bank_ids = [b.id for b in bank_ids]
            
        if _debug:
            _logger.info("########### FOUND : %s,%s,%s,%s,%s,%s,%s,%s",custom_footer, phone, fax, email, website, vat, company_registry, bank_ids)
        footer = self.pool.get('res.company').onchange_footer(cr, uid, ids, custom_footer, phone, fax, email, website, vat, company_registry, bank_ids, context)
        if _debug:
            _logger.info("########### FOOTER : %s", footer['value'])
            #{'value': {'rml_footer': u'Email: info@yourcompany.com | Website: www.yourcompany.com\nBank Accounts: NBC Bank A/C No. 028103002124 (D: 028103002124, NBC Bank A/C No. 028103002112 (S: 028103002112, NMB Bank A/C No. 6O53500091 (Bas: 6O53500091', 'rml_footer_readonly': u'Email: info@yourcompany.com | Website: www.yourcompany.com\nBank Accounts: NBC Bank A/C No. 028103002124 (D: 028103002124, NBC Bank A/C No. 028103002112 (S: 028103002112, NMB Bank A/C No. 6O53500091 (Bas: 6O53500091'}}
        
        if _debug:
            _logger.info("########### UPDATING FOOTER") 
        company_pool.write(cr, uid, company_ids, footer['value'], context)
        
isf_tosa_data()
